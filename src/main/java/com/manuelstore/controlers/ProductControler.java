package com.manuelstore.controlers;

import com.manuelstore.models.Product;
import com.manuelstore.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/Potato/Products")
public class ProductControler{

    @Autowired
    public ProductRepository productRepository;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody @Valid Product product, BindingResult result){

        if(result.hasErrors())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList()));
        return ResponseEntity.ok(productRepository.save(product));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity put(@RequestBody Product product){
        Product p = productRepository.save(product);

        return ResponseEntity.ok(p);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(){
        return ResponseEntity.ok(productRepository.findAll());
    }


    @GetMapping(params = "id", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(Integer id){
        return ResponseEntity.ok(productRepository.findById(id));
    }


    @DeleteMapping(params = "id", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteById( Integer id){
        productRepository.deleteById(id);
        return ResponseEntity.ok("{'response':'ok'}");
    }


}
