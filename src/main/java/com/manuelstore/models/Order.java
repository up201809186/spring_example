package com.manuelstore.models;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Table(name = "Request")
@Entity
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private LocalDateTime dateOfRequest;

    @Column
    private String nameOfBuyer;

    @Column
    private StateOfRequest stateOfRequest;

    @Column
    private Integer totalPrice;

    @Column
    private Integer fare;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Column
    private List<Item> items;
}
