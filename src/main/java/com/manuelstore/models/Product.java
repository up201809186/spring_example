package com.manuelstore.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "Product")
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //@NotNull(message = "Put the damn code!")
    @NotNull(message = "{product.code.notnull}")
    @Column
    private String code;

    //@NotNull(message = "Put the damn name!")
    @NotNull(message = "{product.name.notnull}")
    @Column
    private String name;

    //@NotNull(message = "Put the damn description!")
    @NotNull(message = "{product.description.notnull}")
    @Column
    private String description;

    // @NotNull(message = "Put the damn stock!")
    @NotNull(message = "{product.stock.notnull}")
    @Column
    private Integer stock;

    //@NotNull(message = "Put the damn price!")
    @NotNull(message = "{product.price.notnull}")
    @Column
    private Double price;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Column
    private List<Attribute> attributes;

}
