package com.manuelstore.models;

public enum StateOfRequest {
    NEW,APPROVED,DELIVERED,CANCELED;
}
