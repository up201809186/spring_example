package com.manuelstore.test.java.com.manuelstore.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.manuelstore.models.Product;
import com.manuelstore.repository.ProductRepository;

import org.junit.jupiter.api.Test;
//import org.junit.runner
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
//@RunWit
public class ProductControllerTest {

    @MockBean
    ProductRepository productRepository;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void shouldCreateProduct() throws Exception {
        Product product = new Product();
        product.setName("Potatos");
        product.setDescription("Bag of Potatoes");
        product.setCode("ThePotato🥔");
        product.setStock(100);
        product.setPrice(0.12);

        mockMvc.perform(MockMvcRequestBuilders.post("/Potato/Products/")
                .content(new ObjectMapper().writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }


}
